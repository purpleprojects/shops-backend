package org.sid.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity //Entity Of Shop
@Table(name="shop")
public class Shop implements Comparable<Shop>{
	
	@Id //@GeneratedValue I comented it for testing
	private Long id;
	private String shopName;
	private float lat;
	private float longt;
	
	//I made this attribute transient in order to not create it in the database
	@Transient
	private int distance;
	
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	@OneToMany(mappedBy = "shop")
	Set<UserShop> userShop;

	
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	
	public float getLongt() {
		return longt;
	}
	public void setLongt(float longt) {
		this.longt = longt;
	}
	
	public Long getId() {
		return id;
	}
	public Shop() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Shop(Long id, String shopName, float lat, float longt) {
		super();
		this.id = id;
		this.shopName = shopName;
		this.longt = longt;
		this.lat = lat;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	//I use this method to compare distances (basing on the position of the user)
	//in order to sort the shops by distance
	@Override
	public int compareTo(Shop o) {
		if(distance == o.getDistance())
	        return 0;
	    else if(distance>o.getDistance())
            return 1;
	    else
	        return -1;
	}

	
}

