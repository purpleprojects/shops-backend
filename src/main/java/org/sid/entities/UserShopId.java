package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserShopId implements Serializable{
	
	@Column(name = "user_id")
    Long userId;
 
    @Column(name = "shop_id")
    Long shopId;

	public UserShopId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserShopId(Long userId, Long shopId) {
		super();
		this.userId = userId;
		this.shopId = shopId;
	}


}



