package org.sid.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;

@Entity //Entity Of User <=> Shop (Relation between a User and a Shop
@Table(name="user_shop")
public class UserShop   {
	/**
	 * 
	 */
   
	@EmbeddedId 
	UserShopId id;
	
	
    @ManyToOne
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    AppUser user;
 
    @ManyToOne
    @MapsId("shop_id")
    @JoinColumn(name = "shop_id", nullable = false, insertable = false, updatable = false)
    Shop shop;
 
    @Column(name = "created_date")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    Date created_at = new Date();

    @Column(name = "relation_type")
    String relationType;

	public UserShop(UserShopId id,AppUser user, Shop shop, String relationType) {
		super();
		this.id = id;
		this.user = user;
		this.shop = shop;
		this.relationType = relationType;
	}

	public UserShop() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserShopId getId() {
		return id;
	}

	public void setId(UserShopId id) {
		this.id = id;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}
    
    
    
}

