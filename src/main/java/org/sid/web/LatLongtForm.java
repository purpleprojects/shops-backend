package org.sid.web;

public class LatLongtForm {
	private String lat;
	private String longt;
	public LatLongtForm(String lat, String longt) {
		super();
		this.lat = lat;
		this.longt = longt;
	}
	public LatLongtForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLongt() {
		return longt;
	}
	public void setLongt(String longt) {
		this.longt = longt;
	}

	
}
