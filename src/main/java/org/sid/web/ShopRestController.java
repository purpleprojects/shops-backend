package org.sid.web;

import java.util.Collection;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import org.sid.entities.AppUser;
import org.sid.entities.Shop;
import org.sid.service.AccountService;
import org.sid.service.UserShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//Creating a REST API To communicate with the client side
@RestController
public class ShopRestController {
		
	private final Path rootLocation = Paths.get("C:\\Users\\Alouane\\Desktop\\workspace\\shops\\src\\main\\resources\\static\\shops_images");
	
	@Autowired
	private AccountService accountService;

	@Autowired
	private UserShopService userShopService;
	
	//Method to add a prefered shop for a specific user
	@PostMapping("/shops/addPreferedShop")
	public void savePreferedShop(@RequestBody ShopForm shopPrefered, Principal principal){
		System.out.println("prefered shop id : "+shopPrefered);
		userShopService.savePreferedShopForUser(shopPrefered.getId(), principal.getName());
	}
	
	//Method to add a disliked shop for a specific user
	@PostMapping("/shops/addDislikedShop")
	public void saveDislikedShop(@RequestBody ShopForm shopDisliked, Principal principal){
		userShopService.saveDislikedShopForUser(shopDisliked.getId(), principal.getName());
	}
	
	//Method to get a list of prefered shops for a specific user
	@GetMapping("/shops/prefered")
	public Collection<Shop> listPreferedShops(Principal principal){
		Collection<Shop> preferedShops = userShopService.listShopsByUserAndType(principal.getName(),"prefered");
		return preferedShops;
	}
	
	//Method to to delete a prefered shop for user
	@PostMapping("/shops/deletePreferedShop")
	public void deletePreferedShop(@RequestBody ShopForm shopPrefered, Principal principal){
		AppUser user = accountService.findUserByEmail(principal.getName());
		userShopService.deletePreferedShop(user.getId(),shopPrefered.getId());
	}
	
	//Method to get a list of nearBy shops that are not prefered either disliked for a specific user
	@GetMapping("/shops/near")
	public Collection<Shop> listNearByShops(@RequestParam("lat") float lat,@RequestParam("longt") float longt, Principal principal){
		Collection<Shop> allShops = userShopService.listNearByShops(principal.getName(), lat, longt);
		return allShops;
	}
	
	//Method to get the image of a specific shop
	@GetMapping(value="/images/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> getFile(@PathVariable String filename) throws MalformedURLException {
		Path file = rootLocation.resolve(filename);
        Resource resource = new UrlResource(file.toUri());
		Resource file1 = resource;
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file1.getFilename() + "\"")
				.body(file1);
	}
	
}
