package org.sid;


import org.sid.dao.ShopRepository;
import org.sid.entities.AppRole;
import org.sid.entities.Shop;
import org.sid.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class ShopsApplication implements CommandLineRunner {
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ShopRepository shopRepository;

	
	public static void main(String[] args) {
		SpringApplication.run(ShopsApplication.class, args);
	}
	
	@Bean
	public BCryptPasswordEncoder getBCPE() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void run(String... args) throws Exception {
		
		//Clear Role table
		accountService.clearRole();
		
		//Creating role USER
		accountService.saveRole(new AppRole (1L,"USER"));
		
		//Turning on event scheduler in mysql
		jdbcTemplate.update(" SET GLOBAL event_scheduler = ON");
		
		//Droping the event if it exist
		jdbcTemplate.update("DROP EVENT IF EXISTS event_delete_disliked_record;");
		
		//Creating event for deleting disliked shops after 2 hours
		jdbcTemplate.update("CREATE EVENT event_delete_disliked_record ON SCHEDULE EVERY 1 MINUTE DO DELETE FROM user_shop WHERE relation_type='disliked' AND created_date < DATE_SUB(NOW() , INTERVAL 2 HOUR);");
		
		shopRepository.deleteAll();
		Shop shop1 = new Shop(1L,"Shopping Dakhla",23.68477f,-15.95798f);
		shopRepository.save(shop1);
		Shop shop2 = new Shop(2L,"Taza Store",34.21f,-4.01f);
		shopRepository.save(shop2);
		Shop shop3 = new Shop(3L,"Khouribga Shop",32.88108f,-6.9063f);
		shopRepository.save(shop3);
		Shop shop4 = new Shop(4L,"Rabat Store",34.01325f,-6.83255f);
		shopRepository.save(shop4);
		Shop shop5 = new Shop(5L,"Casa Negra Shop",33.58831f,-7.61138f);
		shopRepository.save(shop5);
		Shop shop6 = new Shop(6L,"Settat Store",33.00103f,-7.61662f);
		shopRepository.save(shop6);
		Shop shop7 = new Shop(7L,"Ayoun Store",34.58319f,-2.50612f);
		shopRepository.save(shop7);
		Shop shop8 = new Shop(8L,"Shopping Nador",35.16813f,-2.93352f);
		shopRepository.save(shop8);
		Shop shop9 = new Shop(9L,"Ifrane Shopping",31.70217f,-6.3494f);
		shopRepository.save(shop9);
		Shop shop10 = new Shop(10L,"Store Guelmim",28.98696f,-10.05738f);
		shopRepository.save(shop10);
		Shop shop11 = new Shop(11L,"Store Eljadida",33.25492f,-8.50602f);
		shopRepository.save(shop11);
		Shop shop12 = new Shop(12L,"Tanger Shop",35.76727f,-5.79975f);
		shopRepository.save(shop12);
		Shop shop13 = new Shop(13L,"Here Oujda",34.68053f,-1.90764f);
		shopRepository.save(shop13);
		Shop shop14 = new Shop(14L,"Kenitra Shop",34.26101f,-6.5802f);
		shopRepository.save(shop14);
		Shop shop15 = new Shop(15L,"Ouarzazat Store",30.91894f,-6.89341f);
		shopRepository.save(shop15);
		Shop shop16 = new Shop(16L,"Best in Kech",31.63416f,-7.99994f);
		shopRepository.save(shop16);
		Shop shop17 = new Shop(17L,"Fes Shop",34.03715f,-4.9998f);
		shopRepository.save(shop17);
		Shop shop18 = new Shop(18L,"Agadir for Shopping",30.42018f,-9.59815f);
		shopRepository.save(shop18);
		Shop shop19 = new Shop(19L,"Asfi Store",32.29939f,-9.23718f);
		shopRepository.save(shop19);
		Shop shop20 = new Shop(20L,"Store Houceima",35.25165f,-3.93723f);
		shopRepository.save(shop20);

	}

}
