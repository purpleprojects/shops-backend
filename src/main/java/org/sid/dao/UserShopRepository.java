package org.sid.dao;


import javax.transaction.Transactional;
import org.sid.entities.UserShop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserShopRepository  extends JpaRepository <UserShop, Long> {
	
	
	@Modifying 
	@Transactional 
	//Delete relation between user and a shop basing on the type of relation
	//we use it only for prefered relation (because we delete disliked relation with an event scheduler)
	//but I let it general in case in the future we want to delete a disliked relation manually 
	@Query(value = "DELETE FROM user_shop WHERE user_id = ?1 AND shop_id = ?2 AND relation_type = ?3",nativeQuery = true)
	public void deleteByUserAndShopAndType(Long userId, Long shopId,String type);

}
