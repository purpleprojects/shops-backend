package org.sid.dao;

import java.util.Collection;
import org.sid.entities.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShopRepository extends JpaRepository <Shop, Long> {

//Find shops by there relation to a specific user (shops disliked or prefered)
@Query(value = "SELECT * FROM shop s JOIN user_shop us ON s.id = us.shop_id WHERE us.user_id = ?1 AND us.relation_type LIKE ?2",nativeQuery = true)
public Collection<Shop> findAllByUserAndType(Long userId, String typeRelation);

//Find all shops that are not disliked and prefered, in order to display these shops in near by list
//Because we should not display a prefered or a disliked shop in the list of nearBy shops
@Query(value = "select * from shop ss where not exists (SELECT * FROM user_shop us WHERE us.user_id = ?1 and us.shop_id = ss.id)",nativeQuery = true)
public Collection<Shop> findAllNear(Long userId);

}
