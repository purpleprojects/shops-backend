package org.sid.service;

import java.util.Collection;
import org.sid.entities.Shop;

public interface UserShopService {

	public void savePreferedShopForUser(Long preferedShopId, String email);
	public void saveDislikedShopForUser(Long dislikedShopId, String email);
	public Collection<Shop> listShopsByUserAndType(String email, String typeRelation);
	public Collection<Shop> listNearByShops(String useName,float userLat,float userLongt);
	public void deletePreferedShop(Long userId, Long shopPreferedId);

}
