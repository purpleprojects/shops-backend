package org.sid.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.sid.dao.ShopRepository;
import org.sid.dao.UserRepository;
import org.sid.dao.UserShopRepository;
import org.sid.entities.AppUser;
import org.sid.entities.Shop;
import org.sid.entities.UserShop;
import org.sid.entities.UserShopId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserShopServiceImpl implements UserShopService {

	public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
    public static boolean ASC = true;
    public static boolean DESC = false;

	@Autowired
	private UserRepository userRepository;
		
	@Autowired
	private ShopRepository shopRepository;
	
	@Autowired
	private UserShopRepository userShopRepository;
	
	@Autowired
	private AccountService accountService;

	@Override
	public void savePreferedShopForUser(Long preferedShopId, String email) {
		Shop shopPrefered = shopRepository.findById(preferedShopId).orElse(null);
		AppUser user = userRepository.findByEmail(email);
		UserShopId id= new UserShopId(user.getId(),shopPrefered.getId());
        UserShop userShop = new UserShop(id,user,shopPrefered,"prefered");
        userShopRepository.save(userShop);
	}

	@Override
	public void saveDislikedShopForUser(Long dislikedShopId, String email) {
		Shop shopDisliked = shopRepository.findById(dislikedShopId).orElse(null);
		AppUser user = userRepository.findByEmail(email);
		UserShopId id= new UserShopId(user.getId(),shopDisliked.getId());
        UserShop userShop = new UserShop(id,user,shopDisliked,"disliked");
        userShopRepository.save(userShop);
	}
	
	@Override
	public Collection<Shop> listShopsByUserAndType(String email, String typeRelation){
		AppUser user = userRepository.findByEmail(email);
		Collection<Shop> shops = shopRepository.findAllByUserAndType(user.getId(),typeRelation);
		return shops;
	}
	@Override
	public Collection<Shop> listNearByShops(String userName,float userLat,float userLongt){			
		AppUser user = accountService.findUserByEmail(userName);
		List<Shop> allShops = new ArrayList<>(shopRepository.findAllNear(user.getId()));

		for (Shop shop : allShops){
		    double latDistance = Math.toRadians(userLat - shop.getLat());
		    double lngDistance = Math.toRadians(userLongt - shop.getLongt());
		    
		    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
		  	      + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(shop.getLat()))
		  	      * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

	  	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

	  	    int distanceUserShop = (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c));
	  	  shop.setDistance(distanceUserShop);  
		}
		Collections.sort((List<Shop>) allShops);
        return allShops;
	}
	
	@Override
	public void deletePreferedShop(Long userId, Long shopPreferedId){
		userShopRepository.deleteByUserAndShopAndType(userId,shopPreferedId,"prefered");
	}
}
